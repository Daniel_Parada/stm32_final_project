/*
 * protocol.h
 *
 *  Created on: May 27, 2023
 *      Author: Daniel
 */
#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

#include <stdint.h>


#define PROTOCOL_PREAMBLE '*'
#define PROTOCOL_POSAMBLE '#'

typedef enum protocol_operation_{
	OPERATION_GET 	= 'G',
	OPERATION_SET 	= 'S',


} protocol_operation_t;

typedef enum protocol_element_{
	ELEMENT_DOOR 		= 'D',
	ELEMENT_TEMPERATURE = 'T',
	ELEMENT_VERSION 	= 'F',
	ELEMENT_HEATER		= 'H',
	ELEMENT_SPEED		= 'S',
	ELEMENT_UPDATE		= 'U',

} protocol_element_t;

typedef struct rx_packet_{
	uint8_t operation;
	uint8_t element;
	uint8_t value;

} __attribute__((__packed__)) rx_packet_t;

#endif /* INC_PROTOCOL_H_ */
