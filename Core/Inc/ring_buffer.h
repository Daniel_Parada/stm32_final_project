/**
  ****************************************************************
  * @file 		ring_buffer.h
  * @author		daniel.parada@utp.edu.co
  * @brief		ring buffer's functions
  * @date		05-26-23
  * @version	v.1.0
  *****************************************************************
 */



#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

#include "stdlib.h"
#include <stdint.h>


typedef struct ring_buffer_ {

	uint8_t	*buffer;
	size_t 	head;
	size_t	tail;
	size_t	max;
	uint8_t	full;

} ring_buffer_t;





uint8_t ring_buffer_init(ring_buffer_t *, uint8_t *, size_t);
uint8_t ring_buffer_empty(volatile ring_buffer_t *);
void  ring_buffer_put(volatile ring_buffer_t *, uint8_t );
uint8_t  ring_buffer_get(volatile ring_buffer_t *, uint8_t *);
size_t ring_buffer_size(ring_buffer_t *);


#endif
