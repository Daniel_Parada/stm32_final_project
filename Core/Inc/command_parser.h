/*
 * command_parser.h
 *
 *  Created on: May 27, 2023
 *      Author: Daniel
 */

//#ifndef INC_COMMAND_PARSER_H_
//#define INC_COMMAND_PARSER_H_
#ifndef __COMMAND_PARSER_INC_
#define __COMMAND_PARSER_INC_


#include <stdint.h>
#include <stdlib.h>
#include "protocol.h"

void parse_command(uint8_t *);

#endif /* INC_COMMAND_PARSER_H_ */
