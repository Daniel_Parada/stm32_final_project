/*
 * command_parser.c
 *
 *  Created on: May 27, 2023
 *      Author: Daniel
 */



#include "main.h"
#include "command_parser.h"
#include "stm32f4xx_hal.h"
#include "UART_driver.h"
#include <string.h>

#include <string.h>
#include <stdio.h>

extern uart_driver_t uart_driver;
extern TIM_HandleTypeDef htim1;


typedef enum bmp280_register_address_{
	BMP280_REG_ERROR 			= 0x00,
	BMP280_REG_CAL_START_ADDR 	= 0x88,
	BMP280_REG_ID 				= 0xD0,
	BMP280_REG_RESET  			= 0xE0,
	BMP280_REG_STATUS 			= 0xF3,
	BMP280_REG_CTRL_MEAS 		= 0xF4,
	BMP280_REG_CONFIG		 	= 0xF5,
	BMP280_REG_PRESS_MSB 		= 0xF7,
	BMP280_REG_PRESS_LSB 		= 0xF8,
	BMP280_REG_PRESS_XLSB 		= 0xF9,
	BMP280_REG_TEMP_MSB 		= 0xFA,
	BMP280_REG_TEMP_LSB 		= 0xFB,
	BMP280_REG_TEMP_XLSB 		= 0xFC
} bmp_register_address_t;


const uint8_t ack_message[]	 		= "*ACK#";
const uint8_t nack_message[] 		= "*NACK#";
const uint8_t FW_VERSION[]	 		= "*V1.0.20230603#";
const uint8_t open_door[]		 	= "*D0#";
const uint8_t close_door[]	 		= "*D1#";
const uint8_t on_state_heater[]	 	= "*H0#";
const uint8_t off_state_heater[]	= "*H1#";
char AUX_DOOR[] 		= "*D0#";
char AUX_HEATER[]		= "*H0#";
char AUX_TEMPERATURE[]  = "*T000.00#";
char AUX_SPEED[]		= "*F000#";
char UPDATE[23];

#define PROTOCOL_GET 	'G'
#define PROTOCOL_SET 	'S'


int32_t raw_temperature;
float teperature_in_celcius;
uint8_t Read_Data[3];


uint8_t process_set_command(rx_packet_t *rx_packet){

	uint8_t ret_val = 1;
	switch(rx_packet -> element){

		case ELEMENT_DOOR:

			if(rx_packet -> value == '0'){
				HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_RESET);
			}
			else if(rx_packet -> value == '1'){
				HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_SET);
			}
			else{
				uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			}
			break;
		case ELEMENT_HEATER:
			if(rx_packet -> value == '0'){
				HAL_GPIO_WritePin(HEATER_GPIO_Port, HEATER_Pin, GPIO_PIN_RESET);
			}
			else if(rx_packet -> value == '1'){
				HAL_GPIO_WritePin(HEATER_GPIO_Port, HEATER_Pin, GPIO_PIN_SET);
			}
			else{
				uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			}
			break;

		case ELEMENT_SPEED:
			if(rx_packet -> value == '0'){
				htim1.Instance -> CCR1 = 0;
			}
			else if(rx_packet -> value == '2'){
				htim1.Instance -> CCR1 = 250;
			}
			else if(rx_packet -> value == '5'){
				htim1.Instance -> CCR1 = 500;
			}
			else if(rx_packet -> value == '7'){
				htim1.Instance -> CCR1 = 750;
			}
			else if(rx_packet -> value == '1'){
				htim1.Instance -> CCR1 = 999;
			}
			break;

		default:
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			ret_val = 0;
			break;

	}
	return(ret_val);
}

uint8_t process_get_command(rx_packet_t *rx_packet){

	uint8_t ret_val = 1;
	switch(rx_packet -> element){

		case ELEMENT_VERSION:
			uart_driver_send(&uart_driver, (uint8_t *)FW_VERSION, sizeof(FW_VERSION) - 1);
			break;

		case ELEMENT_DOOR:
			if(HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == 0){
				uart_driver_send(&uart_driver, (uint8_t *)open_door, sizeof(open_door) - 1);


			}else{
				uart_driver_send(&uart_driver, (uint8_t *)close_door, sizeof(close_door) - 1);
				memcpy(AUX_DOOR, close_door, sizeof(close_door));

			}
			strcat(UPDATE, AUX_DOOR);

			break;
		case ELEMENT_HEATER:
			//uart_driver_send(&uart_driver, (uint8_t *)heater_state, sizeof(heater_state ) -1);
			if(HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == 0){
				uart_driver_send(&uart_driver, (uint8_t *)on_state_heater, sizeof(on_state_heater) - 1);
				memcpy(AUX_HEATER, on_state_heater, sizeof(on_state_heater));

			}else{
				uart_driver_send(&uart_driver, (uint8_t *)off_state_heater, sizeof(off_state_heater) - 1);
				memcpy(AUX_HEATER, off_state_heater, sizeof(off_state_heater));
			}


			break;
		case ELEMENT_TEMPERATURE:
			 bmp280_read_register(BMP280_REG_TEMP_MSB, Read_Data);
			 //Called functions temperature
			 raw_temperature = bmp280_get_raw_temperature(Read_Data);
			 teperature_in_celcius = bmp280_temperature_to_degrees(raw_temperature);
			 printf("*T%.2f#\r\n", teperature_in_celcius);
			 break;

		case ELEMENT_SPEED:
			printf("*%.0f#\r\n", htim1);
			sprintf(AUX_SPEED, "*%.0f#", htim1);

			 break;

		case ELEMENT_UPDATE:

			bmp280_read_register(BMP280_REG_TEMP_MSB, Read_Data);
						 //Called functions temperature
			raw_temperature = bmp280_get_raw_temperature(Read_Data);
			teperature_in_celcius = bmp280_temperature_to_degrees(raw_temperature);
			sprintf(AUX_TEMPERATURE, "*fT%.2f#", teperature_in_celcius);

			if(HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == 0){
				memcpy(AUX_DOOR, open_door, sizeof(open_door));

			}else{
				memcpy(AUX_DOOR, close_door, sizeof(close_door));
			}

			if(HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == 0){
				memcpy(AUX_HEATER, on_state_heater, sizeof(on_state_heater));

			}
			else{
				memcpy(AUX_HEATER, off_state_heater, sizeof(off_state_heater));
			}


			strcpy(UPDATE, AUX_TEMPERATURE);
			strcat(UPDATE, AUX_DOOR);
			strcat(UPDATE, AUX_HEATER);
			strcat(UPDATE, AUX_SPEED);

			uart_driver_send(&uart_driver, (uint8_t *)UPDATE, sizeof(UPDATE));
			memset(UPDATE, 0, sizeof(UPDATE));

		default:

			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			ret_val = 0;
			break;

	}


	return(ret_val);
}


void parse_command(uint8_t *data)
{

	rx_packet_t *packet = (rx_packet_t *) data;
	if (packet -> operation == PROTOCOL_GET) {
		if (!process_get_command(packet)) {

		}
	}

	else if (packet -> operation == PROTOCOL_SET) {

       if (!process_set_command(packet)) {
    	   uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
       }
       else{
           uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message) - 1);
       }
	}
	else {
        uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
	}

}
