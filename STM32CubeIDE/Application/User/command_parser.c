/*
 * command_parser.c
 *
 *  Created on: May 27, 2023
 *      Author: Daniel
 */



#include "main.h"
#include "command_parser.h"
#include "stm32f4xx_hal.h"
#include "UART_driver.h"
#include <string.h>
#include "Components/ili9341/ili9341.h"
#include "host_comm_manager.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include <stdlib.h>
#include <stdio.h>
#include "ring_buffer.h"
#include "protocol.h"



extern uart_driver_t uart_driver;
extern TIM_HandleTypeDef htim1;


typedef enum bmp280_register_address_{
	BMP280_REG_ERROR 			= 0x00,
	BMP280_REG_CAL_START_ADDR 	= 0x88,
	BMP280_REG_ID 				= 0xD0,
	BMP280_REG_RESET  			= 0xE0,
	BMP280_REG_STATUS 			= 0xF3,
	BMP280_REG_CTRL_MEAS 		= 0xF4,
	BMP280_REG_CONFIG		 	= 0xF5,
	BMP280_REG_PRESS_MSB 		= 0xF7,
	BMP280_REG_PRESS_LSB 		= 0xF8,
	BMP280_REG_PRESS_XLSB 		= 0xF9,
	BMP280_REG_TEMP_MSB 		= 0xFA,
	BMP280_REG_TEMP_LSB 		= 0xFB,
	BMP280_REG_TEMP_XLSB 		= 0xFC
} bmp_register_address_t;


const uint8_t ack_message[]	 		= "*ACK#";
const uint8_t nack_message[] 		= "*NACK#";
const uint8_t FW_VERSION[]	 		= "*V1.0.20230603#";
const uint8_t open_door[]		 	= "*D0#";
const uint8_t close_door[]	 		= "*D1#";
const uint8_t on_state_heater[]	 	= "*H0#";
const uint8_t off_state_heater[]	= "*H1#";
char AUX_DOOR[] 		= "*D0#";
char AUX_HEATER[]		= "*H0#";
char AUX_TEMPERATURE[8]  ;
char tempe[8];
char UPDATE[17];

extern xQueueHandle messageQ;


uint8_t temperature_state;

#define PROTOCOL_GET 	'G'
#define PROTOCOL_SET 	'S'


int32_t raw_temperature;
float teperature_in_celcius;
uint8_t Read_Data[3];


uint8_t process_set_command(rx_packet_t *rx_packet){

	uint8_t ret_val = 1;
	switch(rx_packet -> element){

		case ELEMENT_DOOR:

			if(rx_packet -> value == '0'){
				HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_RESET);
			}
			else if(rx_packet -> value == '1'){
				HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_SET);
			}
			else{
				uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			}
			break;
		case ELEMENT_HEATER:
			if(rx_packet -> value == '0'){
				HAL_GPIO_WritePin(HEATER_GPIO_Port, HEATER_Pin, GPIO_PIN_RESET);
			}
			else if(rx_packet -> value == '1'){
				HAL_GPIO_WritePin(HEATER_GPIO_Port, HEATER_Pin, GPIO_PIN_SET);
			}
			else{
				uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			}
			break;

		default:
			uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
			ret_val = 0;
			break;

	}
	return(ret_val);
}

uint8_t process_get_command(rx_packet_t *rx_packet) {

	uint8_t ret_val = 1;
	switch (rx_packet->element) {

	case ELEMENT_VERSION:
		uart_driver_send(&uart_driver, (uint8_t*) FW_VERSION,
				sizeof(FW_VERSION) - 1);
		break;

	case ELEMENT_DOOR:
		if (HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == 0) {
			uart_driver_send(&uart_driver, (uint8_t*) open_door,
					sizeof(open_door) - 1);

		} else {
			uart_driver_send(&uart_driver, (uint8_t*) close_door,
					sizeof(close_door) - 1);
			memcpy(AUX_DOOR, close_door, sizeof(close_door));

		}
		strcat(UPDATE, AUX_DOOR);

		break;
	case ELEMENT_HEATER:

		if (HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == 0) {
			uart_driver_send(&uart_driver, (uint8_t*) on_state_heater,
					sizeof(on_state_heater) - 1);
			memcpy(AUX_HEATER, on_state_heater, sizeof(on_state_heater));

		} else {
			uart_driver_send(&uart_driver, (uint8_t*) off_state_heater,
					sizeof(off_state_heater) - 1);
			memcpy(AUX_HEATER, off_state_heater, sizeof(off_state_heater));
		}

		break;

	case ELEMENT_TEMPERATURE:
		bmp280_read_register(BMP280_REG_TEMP_MSB, Read_Data);
		//Called functions temperature
		raw_temperature = bmp280_get_raw_temperature(Read_Data);
		teperature_in_celcius = bmp280_temperature_to_degrees(raw_temperature);
		temperature_state = teperature_in_celcius;

		char buffer_temp[5];

		buffer_temp[0] = '0' + (temperature_state / 10);
		buffer_temp[1] = '0' + (temperature_state % 10);
		buffer_temp[2] = '\0';
		char temp_aux1[] = "*T";
		char temp_aux2[] = ".05#";

		strcpy(tempe, temp_aux1);
		strcat(tempe, buffer_temp);
		strcat(tempe, temp_aux2);

		uart_driver_send(&uart_driver, (uint8_t*) tempe, sizeof(tempe));

	case ELEMENT_UPDATE:

		bmp280_read_register(BMP280_REG_TEMP_MSB, Read_Data);
		//Called functions temperature
		raw_temperature = bmp280_get_raw_temperature(Read_Data);
		teperature_in_celcius = bmp280_temperature_to_degrees(raw_temperature);
		temperature_state = teperature_in_celcius;

		char buffer_temp_aux[5];

		buffer_temp_aux[0] = '0' + (temperature_state / 10);
		buffer_temp_aux[1] = '0' + (temperature_state % 10);
		buffer_temp_aux[2] = '\0';
		char temp_1[] = "*.T";
		char temp_2[] = ".05#";

		strcpy(AUX_TEMPERATURE, temp_1);
		strcat(AUX_TEMPERATURE, buffer_temp_aux);
		strcat(AUX_TEMPERATURE, temp_2);

		if (HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == 0) {
			memcpy(AUX_DOOR, open_door, sizeof(open_door));

		} else {
			memcpy(AUX_DOOR, close_door, sizeof(close_door));
		}

		if (HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == 0) {
			memcpy(AUX_HEATER, on_state_heater, sizeof(on_state_heater));

		} else {
			memcpy(AUX_HEATER, off_state_heater, sizeof(off_state_heater));
		}

		strcpy(UPDATE, AUX_TEMPERATURE);
		strcat(UPDATE, AUX_DOOR);
		strcat(UPDATE, AUX_HEATER);


		uart_driver_send(&uart_driver, (uint8_t *)UPDATE, sizeof(UPDATE)-1);




		default:

		 uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
		 ret_val = 0;
		 break;

	}

	return (ret_val);
}


void parse_command(uint8_t *data)
{

	rx_packet_t *packet = (rx_packet_t *) data;
	if (packet -> operation == PROTOCOL_GET) {
		if (!process_get_command(packet)) {

		}
	}

	else if (packet -> operation == PROTOCOL_SET) {

       if (!process_set_command(packet)) {
    	   uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
       }
       else{
           uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message) - 1);
       }
	}
	else {
        uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
	}

}
