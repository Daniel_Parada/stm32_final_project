#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "stdint.h"

uint8_t temperature;

extern "C"{
	xQueueHandle messageQ;
}
Model::Model() : modelListener(0)
{
	messageQ = xQueueGenericCreate(1, 1, 0);
}

void Model::tick()
{
	if(xQueueReceive(messageQ, &temperature, 0) == pdTRUE){
		modelListener -> setNewTemp(temperature);
	}

}

