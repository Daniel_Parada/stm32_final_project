#include <gui/screen1_screen/Screen1View.hpp>
#include "stm32f4xx_hal.h"

Screen1View::Screen1View()
{
	toggle_image = 0;

	On_Img.setVisible(false);
	On_Img.invalidate();

}

void Screen1View :: touchable_image(touchgfx :: ScalableImage *image, uint8_t touchable){

	if(touchable == 1){
		image -> setTouchable(true);
		image -> invalidate();
	}
	else if(touchable == 0){
		image -> setTouchable(false);
		image -> invalidate();
	}
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View :: updateTemp(uint8_t new_temp){

	textArea1.resizeToCurrentText();
	textArea1.invalidate();

	Unicode :: snprintf(textArea1Buffer, TEXTAREA1_SIZE, "%d", new_temp);

	textArea1.resizeToCurrentText();
	textArea1.invalidate();
}

void Screen1View :: Chages_State(){

	if(toggle_image == 0){

		Off_Img.setVisible(true);
		Off_Img.invalidate();

		On_Img.setVisible(false);
		On_Img.invalidate();
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);

		toggle_image = 1;
	}

	else if(toggle_image == 1){

		Off_Img.setVisible(false);
		Off_Img.invalidate();

		On_Img.setVisible(true);
		On_Img.invalidate();
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);

		toggle_image = 0;
	}
}
