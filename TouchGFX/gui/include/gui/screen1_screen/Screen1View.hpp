#ifndef SCREEN1VIEW_HPP
#define SCREEN1VIEW_HPP

#include <gui_generated/screen1_screen/Screen1ViewBase.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>
#include "stm32f4xx_hal.h"

class Screen1View : public Screen1ViewBase
{
public:
    Screen1View();
    virtual ~Screen1View() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    void updateTemp(uint8_t new_temp);
    void touchable_image(touchgfx :: ScalableImage *image, uint8_t touchable);

    virtual void Chages_State();

    uint8_t toggle_image : 1;

protected:
};

#endif // SCREEN1VIEW_HPP
